use gitlab::api::{issues, projects, Query};
use gitlab::types::{Project, Issue};
use gitlab::Gitlab;
use std::env;
use askama::Template;
use std::fs;
use std::fs::File;
use std::io::Write;

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
    site: &'a Project,
    blogs: &'a Vec<Issue>
}

#[derive(Template)]
#[template(path = "blog.html")]
struct BlogTemplate<'a> {
    site: &'a Project,
    blog: &'a Issue
}

mod filters {
    // Filtre qui transforme du markdown en html
    pub fn mdtohtml<T: std::fmt::Display>(s: T) -> ::askama::Result<String> {
        let s = s.to_string();
        Ok(markdown::to_html(&s))
    }
}


fn main() {
    // Client API
    let token = match env::var_os("GITLAB_PRIVATE_TOKEN") {
        Some(v) => v.into_string().unwrap(),
        None => panic!("$GITLAB_PRIVATE_TOKEN is not set")
    };
    let client: gitlab::Gitlab = Gitlab::new("gitlab.com", token).unwrap();

    // Récupération du projet
    let project_id = match env::var_os("CI_PROJECT_ID") {
        Some(v) => v.into_string().unwrap(),
        None => panic!("$CI_PROJECT_ID is not set")
    };
    let endpoint: gitlab::api::projects::Project<'_> = projects::Project::builder()
        .project(&project_id)
        .build()
        .unwrap();
    let project: Project = endpoint.query(&client).unwrap();
    // println!("Blog {:#?}", project);

    // Récupération des billets
    let endpoint: gitlab::api::issues::ProjectIssues<'_> = issues::ProjectIssues::builder()
        .project(&project_id)
        .build()
        .unwrap();
    let issues: Vec<Issue> = endpoint.query(&client).unwrap();
    // println!("Billets : {:#?}", issues);

    // Génération du html
    fs::remove_dir_all("public").unwrap();
    fs::create_dir_all("public/blogs").unwrap();
    fs::create_dir_all("public/tags").unwrap();
    fs::create_dir_all("public/categories").unwrap();

    let index = IndexTemplate { site: &project, blogs: &issues };
    // println!("{}", index.render().unwrap());
    let mut html_file = File::create("public/index.html").expect("creation failed");
    html_file.write(index.render().unwrap().as_bytes()).expect("write failed");

    for issue in &issues {
        let blog = BlogTemplate { site: &project, blog: &issue };
        let filename = format!("public/blogs/{0}.html", issue.iid);
        // println!("{:#1}", blog.render().unwrap());
        let mut html_file = File::create(&filename).expect("creation failed");
        html_file.write(blog.render().unwrap().as_bytes()).expect("write failed");
    }

}
